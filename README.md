# README #

### What is this repository for? ###

* De Bruijn Sequence Generator
* Generates circular scaffolds


### How to cite ###

If you use this code please cite the following paper:


*Designing Uniquely Addressable Bio-orthogonal Synthetic Scaffolds for DNA and RNA Origami*
Jerzy Kozyra, Alessandro Ceccarelli, Emanuela Torelli, Annunziata Lopiccolo, Jing-Ying Gu, Harold Fellermann, Ulrich Stimming, and Natalio Krasnogor
**ACS Synthetic Biology** 2017 6 (7), 1140-1149
DOI: 10.1021/acssynbio.6b00271 

link:https://pubs.acs.org/doi/full/10.1021/acssynbio.6b00271



### Contribution guidelines ###

* Writing tests
* Code review
* Debug/Add functionality via pull requests

### Who do I talk to? ###

* Benjamin Shirt-Ediss (email: benjamin.shirt-ediss@newcastle.ac.uk)
* Natalio Krasnogor (email: natalio.krasnogor@newcastle.ac.uk)