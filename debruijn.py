import random
from collections import deque

class Edge():
    def __init__(self, i, alphabet, length):
        self.name = self.generate_name(i, alphabet, length)
        self.ins = []
        self.outs = []
        # for Breadth-First-Search
        self.distance = float('inf')
        self.parent = None

    def generate_name(self, i, alphabet, length):
        name = ""
        while i > 0:
            name = alphabet[i % len(alphabet)] + name
            #i /= len(alphabet)          # python2      ( /= performs integer division, which it does not in python3)
            i = int(i / len(alphabet))   # python3
        while len(name) < length:
            name = alphabet[0] + name
        return name

    def get_name(self):
        return self.name

    def matches(self, edge):
        return self.name[1:] == edge.name[:-1]

    def connect(self, edge):
        self.outs.append(edge)
        edge.ins.append(self)

    def disconnect(self, edge):
        if edge in self.outs:
            self.outs.remove(edge)
        if self in edge.ins:
            edge.ins.remove(self)

class Graph():
    def __init__(self, alphabet, length):
        self.start = None
        self.n = length
        self.all_edges = dict()
        for i in range(len(alphabet)**length):
            edge = Edge(i, alphabet, length)
            self.all_edges[edge.name] = edge
        self.connect_all()
        self.set_start()

    def connect_all(self):
        for first in self.all_edges.values():
            for second in self.all_edges.values():
                if first.matches(second):
                    first.connect(second)

    def set_start(self):
        for edge in self.all_edges.values():
            self.start = edge
            break
                
    def removeTabo(self, taboSequences):
        #print taboSequences
        for sequence in taboSequences:
            if len(sequence) >= self.n: #remove one edge containing beginning of the sequence
                if sequence[:self.n] in self.all_edges.keys():
                    self.remove_cycle(sequence[:self.n])
            else:   #remove all edges containing the subsequence
                edges = self.all_edges.keys()
                for edge in edges:
                    if sequence in edge:
                        self.remove_cycle(edge)
        self.set_start()

    def remove_edge(self, edge):
        if isinstance(edge, basestring):
            if edge in all_edges:
                edge = self.all_edges[edge]
            else:
                return
        for other in self.all_edges.values():
            edge.disconnect(other)
            other.disconnect(edge)
        del self.all_edges[edge.name]
    
    def remove_cycle(self, edge):
        if isinstance(edge, basestring):
            if edge in self.all_edges:
                edge = self.all_edges[edge]
            else:
                return
        cycle = self.find_cycle_BFS(edge)
        for edge in cycle:
            self.remove_edge(edge)
        
    def find_cycle_BFS(self, root):
        for edge in self.all_edges.values():
            edge.distance = float('inf')
            edge.parent = None
        root.distance = 0
        queue = deque([root])
        while len(queue) > 0:
            current = queue.popleft()
            for edge in current.outs:
                # cycle found
                if edge == root:
                    cycle = [current]
                    while current.distance != 0:
                        current = current.parent
                        cycle.append(current)
                    return cycle
                # breadth first search
                if edge.distance == float('inf'):
                    edge.distance = current.distance + 1
                    edge.parent = current
                    queue.append(edge)
        return []

    def is_dead_end(self, edge, seen):
        for out in edge.outs:
            if out not in seen:
                return False
        return True

    def get_sequence(self, seen):
        return ''.join(edge.get_name()[0] for edge in seen)

    def traverse_randomly(self, seed):
        random.seed(seed)
        seen = []
        #remaining = self.all_edges.values()[:]      # python2       ([:] is used to cast to list, but raises error in python3)
        remaining = list( self.all_edges.values() ) # python3       

        # start radnom traverse
        current = self.start
        seen.append(current)
        remaining.remove(current)
        while not self.is_dead_end(current, seen):
            random.shuffle(current.outs)
            for out in current.outs:
                if out in seen:
                    continue
                current = out
                seen.append(current)
                remaining.remove(current)
                break
        # complete the traverse with insertions
        while remaining:
            # find insertion point
            while True:
                edge = random.choice(remaining)
                previous = random.choice(edge.ins)
                if previous in seen:
                    index = seen.index(previous)
                    break
            index += 1
            seen.insert(index, edge)
            remaining.remove(edge)
            # repair the right hand side
            while not edge.matches(seen[index + 1]):
                out = random.choice(edge.outs)
                if out in remaining:
                    edge = out
                    index += 1
                    seen.insert(index, edge)
                    remaining.remove(edge)
        return self.get_sequence(seen)


def getRdmDNASequence(n, seed, taboSequences=[]):
    alphabet = "ACGT"
    graph = Graph(alphabet, n)
    if taboSequences:
        graph.removeTabo(taboSequences)
    return graph.traverse_randomly(seed)


if __name__ == '__main__':

    # order k, seed
    print(getRdmDNASequence(7, 900))

    # sequence length = 4^k




